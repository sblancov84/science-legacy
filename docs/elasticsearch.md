# Links

    https://logz.io/blog/elasticsearch-cheat-sheet/

# Create index

    curl -X PUT localhost:9200/myindex

# Add document to index

    curl -X POST -H "Content-Type: application/json" localhost:9200/myindex/_doc/ -d @example.json

# Remove index

    curl -X DELETE -H "Content-Type: application/json" localhost:9200/myindex

# Retrieve index

http://localhost:9200/myindex/_search?pretty

Example of result:
    {
    "took" : 2,
    "timed_out" : false,
    "_shards" : {
        "total" : 1,
        "successful" : 1,
        "skipped" : 0,
        "failed" : 0
    },
    "hits" : {
        "total" : {
        "value" : 1,
        "relation" : "eq"
        },
        "max_score" : 1.0,
        "hits" : [
        {
            "_index" : "myindex",
            "_id" : "VhpJk4MBEnrYCg3JkgVr",
            "_score" : 1.0,
            "_source" : {
            "title" : "titulillo",
            "summary" : "resumencillo",
            "document_id" : "identificador",
            "document_type" : "tipo",
            "authors" : "autores et all",
            "subject_category" : "Categoría premium",
            "keywords" : "llave, clave, etc",
            "found_keywords" : "Found llave"
            }
        }
        ]
    }
    }

## Queries

    POST employees/_search
    {
        "query": {
            "match": {
                "phrase": {
                    "query" : "heuristic"
                }
            }
        }
    }
