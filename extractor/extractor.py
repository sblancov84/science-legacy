import requests
import json
import urllib.request
import shutil
import os

import logging

from elasticsearch import Elasticsearch


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

ES_HOST = os.environ.get('ES_HOST', "localhost")
es = Elasticsearch(f'http://{ES_HOST}:9200')


def download_document(document):
    l_filenames = document['file_name']
    l_links_pdf = document['link_pdf']

    for file_name, link_pdf in zip(l_filenames, l_links_pdf):

        url = f'https://ntrs.nasa.gov{link_pdf}'
        logger.info(f'{url=}')

        DOCUMENT_DIRECTORY = '../documents'
        if not os.path.exists(DOCUMENT_DIRECTORY):
            os.mkdir(DOCUMENT_DIRECTORY)
            logger.info("Creating document directory")
        path_file = os.path.join(DOCUMENT_DIRECTORY, file_name)

        with urllib.request.urlopen(url) as response, open(path_file, 'wb') as out_file:
            shutil.copyfileobj(response, out_file)


def download_documents(l_documents):
    for document in l_documents:
        download_document(document)


def insert_document_to_es(document):
    resp = es.index(index="myindex", id=document['document_id'], document=document)


def insert_documents_to_es(l_documents):
    for document in l_documents:
        insert_document_to_es(document)


def parse_data(data):
    counter = 0
    results = data['results']
    l_documents = []
    for result in results:
        if result['downloadsAvailable']:
            document = dict()

            document['title'] = result.get('title', 'not_found')
            document['document_id'] = result.get('id', 'not_found')
            document['document_type'] = result.get('stiType', 'not_found')
            document["abstract"] = result.get('abstract', 'not_found')
            document["keywords"] = result.get('keywords', 'not_found')

            if 'meta' in result:
                document['authors'] = [author['meta']['author']['name'] for author in result['authorAffiliations']]
            else:
                document['authors'] = "not_found"

            if 'subjectCategories' in result:
                document['subject_category'] = ', '.join(result['subjectCategories'])
            else:
                document['subject_category'] = "not_found"

            if 'downloads' in result:
                document["file_name"] = [download['name'] for download in result['downloads'] if 'name' in download]
            else:
                document["file_name"] = "not_found"

            if 'downloads' in result:
                document["link_pdf"] = [download['links']['pdf'] for download in result['downloads'] if 'links' in download]
            else:
                document["link_pdf"] = "not_found"

            if 'downloads' in result:
                document["link_fulltext"] = [download['links']['fulltext'] for download in result['downloads'] if 'links' in download]
            else:
                document["link_fulltext"] = "not_found"

            document["found_keywords"] = "not_found"
            document["summary"] = "not_found"

            l_documents.append(document)

            counter += 1

    logger.info(f"Total results: {len(results)}")
    logger.info(f"Counter: {counter}")

    return l_documents


def main():
    payload = {
        "stiType": "CONTRACTOR_REPORT",
        "center": ["CDMS"],
        "downloadsAvailable": "true"
    }

    options = {
        'headers':{
            'accept': 'application/json',
            'Content-Type': 'application/json',
        },
        'data': json.dumps(payload)
    }
    url = 'https://ntrs.nasa.gov/api/citations/search'
    response = requests.post(url, **options)

    if response.status_code == 200:
        l_documents = parse_data(response.json())
        insert_documents_to_es(l_documents)
        download_documents(l_documents)
    else:
        print(f"Error: {response.status_code} {response.text}")


if __name__ == "__main__":
    main()
    # TODO - Modify loops with tqdm
