# Science Legacy

This is a PoC for:

https://2022.spaceappschallenge.org/challenges/2022-challenges/science-legacy/details

Application should run on any linux environment. It is tested in Debian 10 and
Ubuntu X.Y.

## Installation

Before to run application on any environment, it must to be installed following
execution dependecies:

* Docker
* Docker Compose


## Run

Run application:

    docker-compose up -d
