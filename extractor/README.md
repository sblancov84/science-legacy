# Extractor

## Run

Place promp in this directory, then:

    docker run --rm -it -v $(pwd):/app -w /app python:3.8-slim-buster bash

Inside docker bash:

    pip install -r requirements.txt
    python extractor.py

Note: Running that docker command allows you to edit code locally and run script
inside container.
