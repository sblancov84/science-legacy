from django.urls import path

from reports.views import ReportsFormView


urlpatterns = [
    path('', ReportsFormView.as_view(), name='reports')
]
