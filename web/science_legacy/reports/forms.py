from django import forms


class ReportsForm(forms.Form):
    query = forms.CharField(widget=forms.Textarea)
