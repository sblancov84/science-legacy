
OpenAPI PDF:
https://sti.nasa.gov/docs/OpenAPI-Data-Dictionary-062021.pdf

Hacer un script que saque la información de la API de la NASA, que está en:

https://ntrs.nasa.gov/api/openapi/#/default/SearchController_postSearch

y obtener la información de la estructura de datos mediante peticiones a esa API.


curl -X 'POST' \
  'https://ntrs.nasa.gov/api/citations/search' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "stiType": "CONTRACTOR_REPORT",
"center": [
    "CDMS"
  ]
}'
