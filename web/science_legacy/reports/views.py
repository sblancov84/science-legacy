from elasticsearch import Elasticsearch

from django.views.generic.edit import FormView
from django.urls import reverse_lazy

from reports.forms import ReportsForm


class ReportsFormView(FormView):
    template_name = 'reports.html'
    form_class = ReportsForm
    success_url = reverse_lazy('reports')

    def post(self, request, *args, **kwargs):
        query = request.POST.get('query')
        options = {
            "results": self.__get_result_from_elasticsearch(query)
        }
        context = self.get_context_data(**options)
        result = self.render_to_response(context)
        return result

    def __get_result_from_elasticsearch(self, query):
        data = {
            "match_phrase": {
                "abstract": {
                    "query": query
                }
            }
        }
        # How to make queries to ES:
        # https://www.elastic.co/guide/en/elasticsearch/client/python-api/master/examples.html
        es = Elasticsearch('http://elasticsearch:9200')
        response = es.search(index="myindex", query=data)
        # See example fields in:
        # https://ntrs.nasa.gov/search?center=CDMS
        results = []
        for result in response["hits"]["hits"]:
            results.append(result["_source"])
        print(results)
        return results
